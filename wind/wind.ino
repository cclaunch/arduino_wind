#include <Arduino.h>
#include <ArduinoJson.h>
#include <Ethernet.h>
#include <PubSubClient.h>

#include <math.h>

/* Anemometer reader and reporter.
 *
 * This sketch for Arduino takes wind measurements from
 * a Davis 6410 Anemometer using algorithms developed for
 * National Weather Service Automated Surface Observing Systems (ASOS).
 * Measurements follow the User's Guide from the ASOS website (https://www.weather.gov/asos/)
 * section 3.2.2.  Measurements are saved to local SD card as well as
 * sent over MQTT messages to a local server both for storage and display.
 *
 * Capture of requirements gleaned from the ASOS user's guide:
 * 3.2.2.1
 * - ASOS continuosly measures wind direction and speed once every second
 * - Five-second wind direction and speed averages are computed from the 1-second measurements
 * - Two-minute averages of direction and speed are computed
 * - Direction rounded to nearest degree
 *
 * Deviations from the guide:
 * - Wind speeds given in miles per hour
 * - Wind speeds less than 2 knots are still reported
 * - Wind speed resolution is to the nearest 10th of a mile per hour
 * - Wind direction is reported to the nearest degree instead of 10 degrees
 * - Data stored locally is only for backup purposes, off-board MQTT subscriber stores permanently
 * - Gusts and other wind variances will be reported by off board software (the MQTT subscriber)
 *
 * Heavily inspired by this guide: http://cactus.io/hookups/weather/anemometer/davis/hookup-arduino-to-davis-anemometer
 *
 */

// hardware
#define WIND_SPEED_PIN 2
#define WIND_DIRECTION_PIN A4

// settings
#define SAMPLE_PERIOD_SEC 5
#define SAMPLE_COUNT 24

// network
byte kLocalMac[] = { 0x90, 0xa2, 0xda, 0x0f, 0xf8, 0xb2 };
const IPAddress kLocalIp(10, 20, 30, 45);
EthernetClient eth;
EthernetUDP eth_udp;

// mqtt
const IPAddress kMqttServerIp(10, 20, 30, 25);
const uint16_t kMqttPort = 1883;
const String kMqttTopicFiveSec = "anemometer/five_second_avg";
const String kMqttTopicTwoMin = "anemometer/two_min_avg";
const size_t msg_capacity = JSON_OBJECT_SIZE(3);
const unsigned int kMqttTimeoutMillis = 5000;
long last_connect_attempt_millis = 0;
DynamicJsonDocument msg_doc(msg_capacity);
PubSubClient pub_client(eth);

// loop control
unsigned int last_publish_millis;

// wind values updated via interrupt
volatile unsigned int rotations;
volatile unsigned int direction_instant;
volatile unsigned int timer_counter;
volatile unsigned int direction_periodic_samples[SAMPLE_PERIOD_SEC];
volatile unsigned int direction_samples[SAMPLE_COUNT];
volatile unsigned int direction_average;
volatile float speed_samples[SAMPLE_COUNT];
volatile float speed_average;
volatile bool is_samples_full;
volatile byte direction_periodic_index;
volatile byte sample_index;

void setup()
{
    is_samples_full = false;
    last_publish_millis = 0;

    // disable interrupts
    cli();

    // for debugging, turn off later
    Serial.begin(9600);

    // set up interrupt to detect rotations
    pinMode(WIND_SPEED_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(WIND_SPEED_PIN), rotationInterrupt, FALLING);

    // set up mqtt client
    pub_client.setServer(kMqttServerIp, kMqttPort);
    pub_client.setKeepAlive(5);

    // set up ethernet and wait
    Ethernet.begin(kLocalMac, kLocalIp);
    delay(1500);

    // enable interrupts
    sei();
}

void connect()
{
    // Loop until we're reconnected
    while (!pub_client.connected())
    {
        Serial.print("Attempting MQTT connection...");

        // Attempt to connect
        if (pub_client.connect("anemometer"))
        {
            Serial.println("connected");
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(pub_client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

void loop()
{
    // check MQTT connection and reconnect if needed
    if (not pub_client.connected())
    {
        connect();
    }

    // 1 Hz loop control
    unsigned int now = millis();

    if ((now - last_publish_millis) > 1000)
    {
        timerInterrupt();
        last_publish_millis = now;
    }

    // pass loop through so mqtt client can do its job
    pub_client.loop();
}

// Method of averaging circular measurements allowing for
// overlap from 359 -> 0 and back. Rounded to nearest whole digit.
// Source: https://stackoverflow.com/a/25579022/1544725
int getAverageBearing(volatile unsigned int array[], unsigned int array_size)
{
    float sun_sin = 0;
    float sun_cos = 0;
    float bearing = 0;
    int counter = 0;

    for (unsigned int i = 0; i < array_size; ++i)
    {
        bearing = array[i] * M_PI / 180;
        sun_sin += sin(bearing);
        sun_cos += cos(bearing);
        counter++;
    }

    int avg_bearing = -1;

    if (counter > 0)
    {
        float bearing_rad = atan2(sun_sin / counter, sun_cos / counter);
        avg_bearing = (int)(bearing_rad * 180.0 / M_PI);

        if (avg_bearing < 0)
        {
            avg_bearing += 360;
        }
    }

    return avg_bearing;
}

void publishInstaneous(volatile int direction, volatile float speed)
{
    msg_doc["speed"] = speed;
    msg_doc["direction"] = direction;

    char buffer[256];
    size_t msg_size = serializeJson(msg_doc, buffer);
    pub_client.publish(kMqttTopicFiveSec.c_str(), buffer, msg_size);
}

void publishAverage()
{
    msg_doc["speed"] = speed_average;
    msg_doc["direction"] = direction_average;

    char buffer[256];
    size_t msg_size = serializeJson(msg_doc, buffer);
    pub_client.publish(kMqttTopicTwoMin.c_str(), buffer, msg_size);
}

// tried this as an actual timer interrupt but it did not play nice with mqtt
void timerInterrupt()
{
    timer_counter++;

    // update wind direction every second
    updateWindDirection();
    direction_periodic_samples[direction_periodic_index] = direction_instant;
    direction_periodic_index++;

    // get the periodic samples every SAMPLE_PERIOD_SEC
    if (timer_counter % SAMPLE_PERIOD_SEC == 0)
    {
        // direction = average of the periodic samples taken
        direction_samples[sample_index] = getAverageBearing(direction_periodic_samples, SAMPLE_PERIOD_SEC);
        direction_periodic_index = 0;

        // based on 1600 revolutions per hour = 1 mph
        speed_samples[sample_index] = rotations * 2.25 / SAMPLE_PERIOD_SEC;

        publishInstaneous(direction_samples[sample_index], speed_samples[sample_index]);

        // calculate the updated averages across the samples
        float speed_sum = 0;

        for (unsigned int i = 0; i < SAMPLE_COUNT; ++i)
        {
            speed_sum += speed_samples[i];
        }

        direction_average = getAverageBearing(direction_samples, SAMPLE_COUNT);
        speed_average = speed_sum / SAMPLE_COUNT;

        // keep a moving index so we don't have to shuffle array data around
        sample_index++;
        if (sample_index >= SAMPLE_COUNT)
        {
            sample_index = 0;

            if (not is_samples_full)
            {
                // from now on we've got a full set to average
                is_samples_full = true;
            }
        }

        // only publish the 2-minute average if we have a full sample set
        if (is_samples_full)
        {
            publishAverage();
        }

        rotations = 0;
        timer_counter = 0;
    }
}

// this interrupt gets called once per revolution of the anemometer cups
void rotationInterrupt()
{
    // no debounce needed as this is a hall effect sensor
    rotations++;
}

void updateWindDirection()
{
    // get the raw value and remap from 1024 counts to 360
    direction_instant = map(analogRead(WIND_DIRECTION_PIN), 0, 1023, 0, 359);
}
